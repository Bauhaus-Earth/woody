package client

import (
	"fmt"
	"io"
	"net/http"
)

type ParserFunc func([]byte, any) error
type Parser func([]byte) error

func Parse(p ParserFunc, v any) Parser {
	return func(payload []byte) error {
		return p(payload, v)
	}
}

func Text(payload []byte, v any) error {
	t, ok := v.(*string)
	if !ok {
		return fmt.Errorf("Type assertion failed, expected *string")
	}
	*t = string(payload)
	return nil
}

func Get(url string, params map[string]string, parser Parser) error {
	req, _ := http.NewRequest("GET", url, nil)
	query := req.URL.Query()
	for k, v := range params {
		query.Add(k, v)
	}
	req.URL.RawQuery = query.Encode()
	resp, err := http.DefaultClient.Do(req)
	payload, err := io.ReadAll(resp.Body)
	err = parser(payload)
	return err
}
