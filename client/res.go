package client

import (
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

// https://fenixservices.fao.org/faostat/api/v1/en/groups Lists the database groups available
// https://fenixservices.fao.org/faostat/api/v1/en/domains/<group> List the databases available for a given group
// https://fenixservices.fao.org/faostat/api/v1/en/dimensions/<database> List parameters available for a given database

const (
	BaseRoute = "https://faostatservices.fao.org/api/v1/en/"
	DataRoute = "data/%s"
	// areaRoute = "definitions/domain/FO/area" endpoint returns not all countries
	areaRoute = "codes/area/%s"
	// elementRoute = "definitions/domain/FO/element" endpoint returns wrong codes
	elementRoute = "codes/element/%s"
	itemRoute    = "definitions/domain/%s/item"
	// itemRoute = "codes/item/FO" //endpoint returns not all items
	yearRoute = "codes/year/%s"
)

type object[D data] struct {
	Data []D `json:"data"`
}

type data interface {
	base | area | element | item | year
}

type base struct {
	Code string
	Name string
}

type area struct {
	Code string `json:"code"`
	Name string `json:"label"`
}

type element struct {
	Code string `json:"code"`
	Name string `json:"label"`
}

type item struct {
	Code string `json:"Item Code"`
	Name string `json:"Item"`
}

type year struct {
	Code string `json:"code"`
	Name string `json:"label"`
}

type Parameter struct {
	Codes  map[string]string
	Values map[string]string
}

func (p Parameter) Keys() []string {
	flag := true
	intKeys := make([]int, 0, len(p.Codes))
	strKeys := make([]string, 0, len(p.Codes))
	for key, value := range p.Codes {
		if k, err := strconv.Atoi(key); err != nil {
			flag = false
		} else {
			intKeys = append(intKeys, k)
		}
		strKeys = append(strKeys, value)
	}
	if flag {
		sort.Ints(intKeys)
		for i := 0; i < len(p.Codes); i++ {
			strKeys[i] = strconv.Itoa(intKeys[i])
		}
	} else {
		sort.Strings(strKeys)
		for i, key := range strKeys {
			strKeys[i] = p.Values[key]
		}
	}
	return strKeys
}

func (p Parameter) CSV() string {
	text := fmt.Sprintf("\"%s\",\"%s\"\n", "code", "name")
	for _, key := range p.Keys() {
		text += fmt.Sprintf("\"%s\",\"%s\"\n", key, p.Codes[key])
	}
	return text
}

func (p Parameter) PPrint() string {
	max := len("code")
	keys := p.Keys()
	for _, key := range keys {
		if len(key) > max {
			max = len(key)
		}
	}
	max += 1
	text := fmt.Sprintf("%s%s%s\n", "code", strings.Repeat(" ", max-4), "name")
	for _, key := range keys {
		text += fmt.Sprintf("%s%s%s\n", key, strings.Repeat(" ", max-len(key)), p.Codes[key])
	}
	return text
}

func (p Parameter) Contains(value string) bool {
	_, ok1 := p.Codes[value]
	_, ok2 := p.Values[value]
	return ok1 || ok2
}

func (p Parameter) Code(value string) (string, error) {
	if !p.Contains(value) {
		return "", fmt.Errorf("value %s not found", value)
	}
	if _, ok := p.Codes[value]; ok {
		return value, nil
	} else {
		return p.Values[value], nil
	}
}

type ParameterKey string

var Area ParameterKey = "area"
var Element ParameterKey = "element"
var Item ParameterKey = "item"
var Year ParameterKey = "year"

var Parameters map[ParameterKey]Parameter

func InitParameters(database string) {
	if Parameters == nil {
		Parameters = map[ParameterKey]Parameter{
			Area:    parameters(BaseRoute+fmt.Sprintf(areaRoute, database), area{}),
			Element: parameters(BaseRoute+fmt.Sprintf(elementRoute, database), element{}),
			Item:    parameters(BaseRoute+fmt.Sprintf(itemRoute, database), item{}),
			Year:    parameters(BaseRoute+fmt.Sprintf(yearRoute, database), year{}),
		}
	}
}

func parameters[D data](route string, data D) Parameter {
	var o object[D]

	// TODO error handling
	Get(route, nil, Parse(json.Unmarshal, &o))

	codes := make(map[string]string)
	values := make(map[string]string)
	for _, v := range o.Data {
		d := base(v)
		codes[d.Code] = d.Name
		values[d.Name] = d.Code
	}
	return Parameter{codes, values}
}
