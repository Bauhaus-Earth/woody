package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"woody/client"
)

const db = "FO"

var database string

var rootCmd = &cobra.Command{
	Use:   "woody",
	Short: "Woody is a downloader for timber production data",
	Long: `
Command line interface for downloading timber production data from the Forestry Production and Trade database
(https://www.fao.org/faostat/en/#data/FO). Use the subcommand download to download data and info to show the avialable
arguments for the download flags area, element, item, and year.
`,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		if _, ok := map[string]bool{"FO": true, "LC": true, "RL": true}[database]; !ok {
			database = db
		}
		client.InitParameters(database)
	},
}

func init() {
	rootCmd.CompletionOptions.DisableDefaultCmd = true
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
