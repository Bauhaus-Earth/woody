package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"regexp"
	"strconv"
	"strings"
	"woody/client"
)

var area string
var element string
var item string
var year string
var areaCs string
var showCodes bool
var showUnits bool
var showFlags bool
var showNotes bool
var nullValues bool
var outputType = "csv"
var datasource = "DB4"

var downloadCmd = &cobra.Command{
	Use:   "download",
	Short: "Download timber production data from FAOSTAT",
	Long: `
Download wood production CSV data from FAOSTAT, matching the given flag arguments. Please see woody info --help to learn
which values are available for the flags area, element, item, and year. These four flags accept a single value or a
comma-separated list as arguments. Arguments may be the code or name. The codes can be expanded with the following
syntax: 1961-1963,2000-2002 equals 1961,1962,1963,2000,2001,2002. 
`,
	Example: "woody download --year \"1961-1970,2000-2010\" --area \"Germany,Austria\"",
	RunE: func(cmd *cobra.Command, args []string) error {
		// TODO check if area_cs is valid - one of "M49", "FAO", "ISO2", "ISO3"
		sep := ","
		paras := make(map[string]string)
		paras["area"] = parse(area, sep, client.Parameters[client.Area])
		paras["element"] = parse(element, sep, client.Parameters[client.Element])
		paras["item"] = parse(item, sep, client.Parameters[client.Item])
		paras["year"] = parse(year, sep, client.Parameters[client.Year])
		paras["area_cs"] = areaCs
		paras["show_codes"] = strconv.FormatBool(showCodes)
		paras["show_unit"] = strconv.FormatBool(showUnits)
		paras["show_flags"] = strconv.FormatBool(showFlags)
		paras["show_notes"] = strconv.FormatBool(showNotes)
		paras["null_values"] = strconv.FormatBool(nullValues)
		paras["output_type"] = outputType
		paras["datasource"] = datasource
		if key, ok := empty(paras); ok {
			return fmt.Errorf("%s is required", key)
		}
		var payload string
		if err := client.Get(client.BaseRoute+fmt.Sprintf(client.DataRoute, database), paras, client.Parse(client.Text, &payload)); err != nil {
			return err
		}
		if output != "" {
			return os.WriteFile(output, []byte(payload), 0644)
		}
		fmt.Printf("%s", payload)
		return nil
	},
}

func init() {
	rootCmd.AddCommand(downloadCmd)
	downloadCmd.Flags().StringVarP(&area, "area", "a", "World + (Total)", "Areas to download")
	downloadCmd.Flags().StringVarP(&element, "element", "e", "Production Quantity", "Elements to download")
	downloadCmd.Flags().StringVarP(&item, "item", "i", "Roundwood", "Items to download")
	downloadCmd.Flags().StringVarP(&year, "year", "y", "1961-2021", "Years to download")
	downloadCmd.Flags().StringVar(&areaCs, "area_cs", "M49", "Area coding system")
	downloadCmd.Flags().BoolVar(&showCodes, "show_codes", false, "Show codes column")
	downloadCmd.Flags().BoolVar(&showUnits, "show_units", false, "Show units column")
	downloadCmd.Flags().BoolVar(&showFlags, "show_flags", false, "Show flags column")
	downloadCmd.Flags().BoolVar(&showNotes, "show_notes", false, "Show notes column")
	downloadCmd.Flags().BoolVar(&nullValues, "null_values", false, "Show null values")
	downloadCmd.Flags().StringVarP(&output, "output", "o", "", "Write output to a file")
	downloadCmd.Flags().StringVarP(
		&database, "database", "d", db, "Database to use, one of FO, LC, or RL",
	)
}

func parse(text string, sep string, para client.Parameter) string {
	return strings.Join(code(removeUnknown(expand(normalize(text, sep)), para), para), sep)
}

func normalize(text, sep string) []string {
	result := strings.Split(text, sep)
	for i, v := range result {
		result[i] = strings.TrimSpace(v)
	}
	return result
}

func expand(values []string) []string {
	pattern := regexp.MustCompile(`\d+(\s+)?-(\s+)?\d+`)
	result := make([]string, len(values))
	for _, v := range values {
		if pattern.MatchString(v) {
			values := normalize(v, "-")
			min, _ := strconv.Atoi(values[0])
			max, _ := strconv.Atoi(values[1])
			if min > max {
				min, max = max, min
			}
			for i := min; i <= max; i++ {
				result = append(result, strconv.Itoa(i))
			}
		} else {
			result = append(result, v)
		}
	}
	return result
}

func removeUnknown(values []string, para client.Parameter) []string {
	result := make([]string, 0)
	for _, v := range values {
		if para.Contains(v) {
			result = append(result, v)
		}
	}
	return result
}

func code(values []string, para client.Parameter) []string {
	result := make([]string, 0)
	for _, v := range values {
		if c, err := para.Code(v); err == nil {
			result = append(result, c)
		}
	}
	return result
}

func empty(paras map[string]string) (string, bool) {
	for k, v := range paras {
		if v == "" {
			return k, true
		}
	}
	return "", false
}
