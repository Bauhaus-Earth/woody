package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"woody/client"
)

var output string

var infoCmd = &cobra.Command{
	Use:   "info [flags] [flag_name]",
	Short: "Print the available arguments for download flags area, element, item, and year",
	Long: `
The subcommand prints the arguments for the download flags area, element, item, and year (see woody download --help).
Calling info without any [flag_name] as an argument prints all available values for all four flags. Only the flag's
values are printed if a [flag_name] is given as an argument.
`,
	Example: "woody info -o area.csv area",
	Args:    cobra.MaximumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		var arg client.ParameterKey
		if len(args) != 0 {
			arg = client.ParameterKey(args[0])
		}
		result := ""
		switch arg {
		case client.Area:
			result += client.Parameters[client.Area].CSV()
		case client.Element:
			result += client.Parameters[client.Element].CSV()
		case client.Item:
			result += client.Parameters[client.Item].CSV()
		case client.Year:
			result += client.Parameters[client.Year].CSV()
		default:
			for k, v := range client.Parameters {
				result += fmt.Sprintf("PARAMETER: %s\n", k)
				result += fmt.Sprintf("%s\n", v.PPrint())
			}
		}
		if output != "" && arg != "" {
			return os.WriteFile(output, []byte(result), 0644)
		}
		fmt.Printf("%s", result)
		return nil
	},
}

func init() {
	rootCmd.AddCommand(infoCmd)
	infoCmd.Flags().StringVarP(&output, "output", "o", "", "Write output to a file")
	infoCmd.Flags().StringVarP(
		&database, "database", "d", db, "Database to use, one of FO, LC, or RL",
	)
}
