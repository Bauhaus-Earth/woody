# Woody

Woody is a downloader for timber production data from [FAOSTAT](https://www.fao.org/faostat/en/#data/FO).

## Overview

Woody provides a command line interface for downloading timber production data from the [Forestry Production and Trade
database](https://www.fao.org/faostat/en/#data/FO).

## Installation

Using woody is easy. Just download the latest release for your operating system from
the [releases page](https://codeberg.org/Bauhaus-Earth/woody/releases).

## Usage
